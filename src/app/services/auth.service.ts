import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsuarioModel } from '../models/usuraio.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = 'https://identitytoolkit.googleapis.com/v1';
  private apikey = 'AIzaSyBbwU6AKy9NmVMMzrC5gf07UtLjlQ13UZg';
  userToken: string;

  constructor(private http: HttpClient) {
    this.leerToken();
   }

  logout() {

  }

  login(usuario: UsuarioModel) {
    const authData = {
      ...usuario,
      returnSecureToken: true
    };
    return this.http.post(
      `${ this.url }/accounts:signInWithPassword?key=${ this.apikey }`,
      authData
    ).pipe(map ( resp => {
      console.log('entro en el mapa del login js');
      this.guardarToken(resp['idToken']);
      return resp;
    })
    );
  }

  nuevoUsuario(usuario: UsuarioModel) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    };

    return this.http.post(
      `${ this.url }/accounts:signUp?key=${ this.apikey }`,
      authData
    ).pipe(map ( resp => {
      console.log('entro en el mapa del login js');
      this.guardarToken(resp['idToken']);
      return resp;
    })
    );
  }

  private guardarToken( idToken ) {

    this.userToken = idToken;
    localStorage.setItem('token', idToken);
  }

  private leerToken() {
    if (localStorage.getItem('token')) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }

    return this.userToken;
  }
}
